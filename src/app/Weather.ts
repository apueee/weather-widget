export class Weather {
    id: number;
    city: string;
    date: string;
    state_name: string;
    temp: string;
    max_temp: string;
    min_temp: string;
    icon: string;
}