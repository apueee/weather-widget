import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { WeatherDetailComponent } from './weather-detail/weather-detail.component';
import { AppRoutingModule } from './/app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { WeatherWidgetComponent } from './weather-widget/weather-widget.component';
import { WeatherSearchComponent } from './weather-search/weather-search.component';

@NgModule({
    declarations: [
        AppComponent,
        MessagesComponent,
        WeatherDetailComponent,
        DashboardComponent,
        WeatherWidgetComponent,
        WeatherSearchComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        AppRoutingModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
