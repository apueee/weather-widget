import { Component, OnInit } from '@angular/core';
import { Weather } from '../Weather';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  weathers: Weather[] = [];

  constructor(private weatherService: WeatherService) {  }

  ngOnInit() {
    this.getWeathers();
  }

  getWeathers(): void {
    this.weatherService.getWeathers()
        .subscribe(weathers => this.weathers = weathers);
  }

}
