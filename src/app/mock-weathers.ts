import { Weather } from './Weather';


export const WEATHERS: Weather[] = [
    {id: 2344116, city: 'Istanbul'},
    {id: 638242, city: 'Berlin'},
    {id: 44418, city: 'London'},
    {id: 565346, city: 'Helsinki'},
    {id: 560743, city: 'Dublin'},
    {id: 9807, city: 'Vancouver'}
]