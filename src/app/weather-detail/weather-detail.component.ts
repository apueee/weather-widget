import {Component, Input, OnInit} from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Weather } from '../Weather';
import { WeatherService } from '../weather.service';

@Component({
    selector: 'app-weather-detail',
    templateUrl: './weather-detail.component.html',
    styleUrls: ['./weather-detail.component.css']
})
export class WeatherDetailComponent implements OnInit {

    public weathers: Weather[];

    constructor(
        private route: ActivatedRoute,
        private weatherService: WeatherService,
        private location: Location
    ) { }

    ngOnInit() {
        this.getAllWeather();
    }

    getAllWeather(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.weatherService.getTodaysWeather(id)
            .subscribe(results => {
                const weathers = [];
                const woeid = results['woeid'];
                const city = results['title'];

                results['consolidated_weather'].forEach( weather => weathers.push({
                    id: woeid,
                    city: city,
                    state_name: weather['weather_state_name'],
                    date: weather['applicable_date'],
                    temp: weather['the_temp'],
                    max_temp: weather['max_temp'],
                    min_temp: weather['min_temp'],
                    icon: "https://www.metaweather.com//static/img/weather/png/64/" + weather['weather_state_abbr'] + ".png"
                }));

                this.weathers = weathers;
            });
    }

    goBack(): void {
        this.location.back();
    }

}
