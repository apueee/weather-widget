import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import {
    debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';

import { Weather } from '../Weather';
import { WeatherService } from '../weather.service';
import { Location } from '../Location';

@Component({
  selector: 'app-weather-search',
  templateUrl: './weather-search.component.html',
  styleUrls: ['./weather-search.component.css']
})
export class WeatherSearchComponent implements OnInit {

  locations$: Observable<Location[]>;

  private searchTerms = new Subject<string>();

  constructor(private weatherService: WeatherService ) { }

  search(term: string): void {
      this.searchTerms.next(term);
  }

  ngOnInit(): void {
      this.locations$ = this.searchTerms.pipe(
          debounceTime(300),

          distinctUntilChanged(),

          switchMap((term: string) => this.weatherService.searchWeather(term))
      );
  }

}


