import {Component, Input, OnInit} from '@angular/core';
import { WeatherService } from '../weather.service';
import {Weather} from '../Weather';

@Component({
    selector: 'app-weather-widget',
    templateUrl: './weather-widget.component.html',
    styleUrls: ['./weather-widget.component.css']
})
export class WeatherWidgetComponent implements OnInit {

    @Input() weather: Weather;

    constructor(private weatherService: WeatherService) { }

    ngOnInit() {
        this.getWeather();
    }
    getWeather(): void {
        // const id = +this.route.snapshot.paramMap.get('id');
        this.weatherService.getTodaysWeather(this.weather.id, this.weather.date)
            .subscribe(weather => this.weather = {
                id: this.weather.date ? this.weather.id : weather['woeid'],
                city: this.weather.date ? this.weather.city : weather['title'],
                state_name: this.weather.date ? weather[0]['weather_state_name'] : weather['consolidated_weather'][0]['weather_state_name'],
                date: this.weather.date ? weather[0]['applicable_date'] : weather['consolidated_weather'][0]['applicable_date'],
                temp: this.weather.date ? weather[0]['the_temp'] : weather['consolidated_weather'][0]['the_temp'],
                max_temp: this.weather.date ? weather[0]['max_temp'] : weather['consolidated_weather'][0]['max_temp'],
                min_temp: this.weather.date ? weather[0]['min_temp'] : weather['consolidated_weather'][0]['min_temp'],
                icon: "https://www.metaweather.com//static/img/weather/png/64/" + (this.weather.date ? weather[0]['weather_state_abbr'] : weather['consolidated_weather'][0]['weather_state_abbr']) + ".png"
            });
    }

}
