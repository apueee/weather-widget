import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Weather } from './Weather';
import { Location} from './Location';
import { WEATHERS } from './mock-weathers';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
    providedIn: 'root'
})

export class WeatherService {
    private weatherUrl = 'http://localhost/weather.php';
    constructor(
        private http: HttpClient,
        private messageService: MessageService) { }

    getWeather(id: number): Observable<Weather> {
        this.messageService.add(`WeatherService: fetched weather id=${id}`);

        return of(WEATHERS.find(weather => weather.id === id ));
    }

    getTodaysWeather(id: number, date: string): Observable<Weather> {
        return this.http.get<Weather>(this.weatherUrl + '?command=location&woeid=' + id + (date ? '&date=' + date.replace(/-/g,'/') : ''));
    }

    searchWeather(term: string): Observable<Location[]> {
        if (!term.trim()) {
            return of([]);
        }
        return  this.http.get<Location[]>(`${this.weatherUrl}?command=search&keyword=${term}`);
    }

    getWeathers(): Observable<Weather[]> {

        this.messageService.add('WeatherService: fetched weathers');

        return of(WEATHERS);

    }
}
